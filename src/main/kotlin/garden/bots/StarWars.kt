package garden.bots

import garden.bots.builders.json
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext

private fun RoutingContext.json(jsonObject: JsonObject) {
  this.response().putHeader("content-type", "application/json;charset=UTF-8").end(jsonObject.encodePrettily())
}

class StarWars : RoutePlugin {

  override fun define(router: Router) {
    router.get("/star-wars").handler { context ->
      context.json(json {
        "message" to DarthVader().getQuote()
      })
    }
  }
}