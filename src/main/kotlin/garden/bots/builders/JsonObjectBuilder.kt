package garden.bots.builders
// see https://gist.github.com/pjozsef/157750001ef56a7dc244302da73211dd
import io.vertx.core.json.JsonObject

class JsonObjectBuilder {
  val json = JsonObject()

  infix fun <B> String.to(other: B) {
    json.put(this, other)
  }
}