package garden.bots

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import io.vertx.core.AbstractVerticle
import io.vertx.core.Vertx
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.kotlin.core.http.HttpServerOptions
import java.util.logging.Level
import java.util.logging.Logger

fun main(args: Array<String>) {
  // https://github.com/eclipse/vert.x/issues/1379#issuecomment-285124962
  //Logger.getLogger("io.vertx.core.impl.BlockedThreadChecker").level = Level.OFF;
  Logger.getLogger("io.vertx.core.impl.BlockedThreadChecker").setLevel(Level.OFF);

  val vertx = Vertx.vertx()
  vertx.deployVerticle(Plugin())

}

/**
 * ## Just for test
 */
class Plugin : AbstractVerticle() {
  override fun start() {

    val envHttpPort = Option.fromNullable(System.getenv("PORT"))

    val httpPort: Int = when(envHttpPort) {
      is None -> 8080
      is Some -> {
        Integer.parseInt(envHttpPort.t)
      }
    }

    val router = Router.router(vertx)
    router.route().handler(BodyHandler.create())

    StarWars().define(router)

    /* === Start the server === */

    vertx.createHttpServer(HttpServerOptions(port = httpPort))
        .requestHandler {
          router.accept(it)
        }
        .listen { ar ->
          when {
            ar.failed() -> println("😡 Houston?")
            ar.succeeded() -> println("😃 🌍 J-Bot-Plugin started on $httpPort")
          }
        }
  }

}