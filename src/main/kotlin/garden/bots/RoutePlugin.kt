package garden.bots

import io.vertx.ext.web.Router

interface RoutePlugin {
  fun define(router: Router)
}